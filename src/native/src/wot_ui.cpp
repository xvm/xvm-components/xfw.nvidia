/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "wot_ui.h"

void WotUi::SwitchVisibility(bool visibility)
{
    //get SF app
    pybind11::object o_i_apploader = pybind11::module::import("skeletons.gui.app_loader").attr("IAppLoader");
    pybind11::object o_apploader = pybind11::module::import("helpers.dependency").attr("instance")(o_i_apploader);
    pybind11::object o_app = o_apploader.attr("getApp")();

    //set app vvisibility
    o_app.attr("component").attr("visible") = visibility;
    o_app.attr("graphicsOptimizationManager").attr("switchOptimizationEnabled")(visibility);

    //fire game/guiVisibility event to change crosshairs visibility
    pybind11::object o_game_event = pybind11::module::import("gui.shared.events").attr("GameEvent")(
        "game/guiVisibility",
        std::map<std::string, bool>{ {"visible", visibility } }
    );
    pybind11::module::import("gui.shared").attr("g_eventBus").attr("handleEvent")(o_game_event, 3); //3 is for battle
}
