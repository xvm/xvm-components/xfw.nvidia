/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <pybind11/pybind11.h>

class WotAvatar {
public:
    WotAvatar() = delete;

    static pybind11::object GetPlayer();

    static pybind11::object GetPlayerAvatar();

    static pybind11::object GetAvatarInputHandler();

    static std::string GetAvatarControllerName();

    static pybind11::object GetAvatarCurrentController();
    static bool EnableAvatarCurrentController(bool enable);

    static bool SwitchAvatarCamera();
};