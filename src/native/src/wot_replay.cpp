/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "wot_replay.h"

void WotReplay::Enable()
{
    enableCameraControl();
    enableReplaySpeed();
    _is_enabled = true;
}

void WotReplay::Disable()
{
    if (!_is_enabled) {
        return;
    }
    disableCameraControl();
    disableReplaySpeed();
}

int WotReplay::GetMaxReplaySpeed()
{
    return _playbackSpeeds.size() - 1;
}

bool WotReplay::IsReplayPlaying()
{
    pybind11::object o_replayctrl = pybind11::module::import("BattleReplay").attr("g_replayCtrl");
    auto result = pybind11::bool_(o_replayctrl.attr("isPlaying"));

    return result;
}

void WotReplay::enableCameraControl()
{
    //disable camera control by replay manager
    pybind11::object o_replayctrl = pybind11::module::import("BattleReplay").attr("g_replayCtrl");
    pybind11::object o_replayctrl_wg = o_replayctrl.attr("_BattleReplay__replayCtrl");
    _replay_controls_camera = pybind11::bool_(o_replayctrl_wg.attr("isControllingCamera"));
    o_replayctrl_wg.attr("isControllingCamera") = false;
}

void WotReplay::enableReplaySpeed()
{
    //get playbackspeeds list
    pybind11::object o_replayctrl = pybind11::module::import("BattleReplay").attr("g_replayCtrl");
    pybind11::sequence o_playbackSpeeds = pybind11::sequence(o_replayctrl.attr("_BattleReplay__playbackSpeedModifiers"));

    //fill vector with it
    _playbackSpeeds.clear();
    for (const auto& val : o_playbackSpeeds) {
        _playbackSpeeds.push_back(pybind11::float_(val));
    }

    //backup speed index
    _replay_initial_speed = getReplaySpeedIndex();
}

void WotReplay::disableCameraControl()
{
    //restore replay manager state
    pybind11::object o_replayctrl = pybind11::module::import("BattleReplay").attr("g_replayCtrl");
    pybind11::object o_replayctrl_wg = o_replayctrl.attr("_BattleReplay__replayCtrl");
    o_replayctrl_wg.attr("isControllingCamera") = _replay_controls_camera;
}

void WotReplay::disableReplaySpeed()
{
    SetReplaySpeedIndex(_replay_initial_speed);
}

int WotReplay::getReplaySpeedIndex()
{
    pybind11::object o_replayctrl = pybind11::module::import("BattleReplay").attr("g_replayCtrl");

    auto result = pybind11::int_(o_replayctrl.attr("_BattleReplay__playbackSpeedIdx"));

    return result;
}

void WotReplay::SetReplaySpeedIndex(int index)
{
    pybind11::object o_replayctrl = pybind11::module::import("BattleReplay").attr("g_replayCtrl");

    o_replayctrl.attr("setPlaybackSpeedIdx")(index);
}
