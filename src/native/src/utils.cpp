/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>

#include <Windows.h>
#include <Shlwapi.h>

#include "utils.h"

///////////////
/// PRIVATE ///
///////////////

static std::vector<HWND> handles;

BOOL CALLBACK get_window_handles_callback(
    __in  HWND hWnd,
    __in  LPARAM lParam
) {
    DWORD pid;
    ::GetWindowThreadProcessId(hWnd, &pid);

    if (pid == lParam) {
        handles.push_back(hWnd);
    }

    return TRUE;
}

///////////////
///  PUBLIC ///
///////////////

std::vector<HWND> Utils::GetWindowHandles()
{
    handles.clear();
    ::EnumWindows(get_window_handles_callback, GetCurrentProcessId());
    return handles;
}

HWND Utils::GetLargestWindow()
{
    LONG max_area = 0;
    HWND result_handle = nullptr;

    std::vector<HWND> handles = GetWindowHandles();
    for (auto& handle : handles)
    {
        RECT rect{};
        if (GetWindowRect(handle, &rect) == FALSE)
        {
            return nullptr;
        }

        LONG area = (rect.right - rect.left)*(rect.bottom - rect.top);
        if (area > max_area)
        {
            max_area = area;
            result_handle = handle;
        }
    }

    return result_handle;
}

std::filesystem::path Utils::GetModuleDirectory(HMODULE hModule)
{
    wchar_t path[MAX_PATH]{};
    GetModuleFileNameW(hModule, path, MAX_PATH);
    PathRemoveFileSpecW(path);

    return std::filesystem::path(path);
}
