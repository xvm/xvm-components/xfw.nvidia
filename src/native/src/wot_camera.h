/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <utility>

#include <pybind11/pybind11.h>

#include <DirectXMath.h>
using namespace DirectX;

#include <AnselSDK.h>


class WotCamera {
public:

    void Enable();

    void Disable();

    ansel::Camera GetCameraSettings();
    void SetCameraSettings(ansel::Camera& cameraSettings);

private:

    //enable function
    void enableCamera();
    void enableProjection();

    //disable functions
    void disableCamera();
    void disableProjection();

    //math convertion
    static std::pair<nv::Vec3, nv::Quat> bigworldToAnsel(pybind11::object& cameraMatrix);
    static pybind11::object anselToBigworld(nv::Vec3 position, nv::Quat rotation);
    static const constexpr int _matrix_dimension = 4;

    //projection object
    pybind11::object _projection_original_function;
    pybind11::object _projection_original_object;
    float _projection_original_farPlane = 0.0f;
    float _projection_original_nearPlane = 0.0f;
    float _projection_original_fov = 0.0f;

    //camera object
    pybind11::object _camera_original;
    pybind11::object _camera_ansel;

    bool _enabled = false;
};
