/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "wot_camera.h"

#include <Windows.h>

void WotCamera::Enable()
{
    if (_enabled) {
        return;
    }

    enableCamera();
    enableProjection();
    _enabled = true;
}

void WotCamera::Disable()
{
    if (!_enabled) {
        return;
    }

    disableCamera();
    disableProjection();
    _enabled = false;
}

ansel::Camera WotCamera::GetCameraSettings()
{
    ansel::Camera camera{};

    //get camera projection
    if (_projection_original_object) {
        camera.farPlane = pybind11::float_(_projection_original_object.attr("farPlane"));
        camera.nearPlane = pybind11::float_(_projection_original_object.attr("nearPlane"));
        camera.fov = static_cast<float>(pybind11::float_(_projection_original_object.attr("fov"))) * 180.0f / M_PI;
    }

    //get camera position and rotation
    if (_camera_ansel) {
        pybind11::object o_camera_matrix = pybind11::module::import("Math").attr("Matrix")(_camera_ansel.attr("matrix"));
        auto [cam_position, cam_rotation] = bigworldToAnsel(o_camera_matrix);
        camera.position = cam_position;
        camera.rotation = cam_rotation;
    }

    return camera;
}

void WotCamera::SetCameraSettings(ansel::Camera& cameraSettings)
{
    //set projection
    if (_projection_original_object) {
        _projection_original_object.attr("farPlane") = cameraSettings.farPlane;
        _projection_original_object.attr("nearPlane") = cameraSettings.nearPlane;
        _projection_original_object.attr("fov") = cameraSettings.fov * M_PI / 180.0f;
    }

    //set position and rotation
    if (_camera_ansel) {
        _camera_ansel.attr("set")(anselToBigworld(cameraSettings.position, cameraSettings.rotation));
    }
}

void WotCamera::enableCamera()
{
    //import BigWorld
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //save old camera
    _camera_original = m_bigworld.attr("camera")();

    //create new camera and set matrix from original one
    _camera_ansel = m_bigworld.attr("FreeCamera")();
    _camera_ansel.attr("spaceID") = _camera_original.attr("spaceID");
    _camera_ansel.attr("set")(_camera_original.attr("matrix"));

    //enable new camera
    m_bigworld.attr("camera")(_camera_ansel);
}

void WotCamera::enableProjection()
{
    //import BigWorld module
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //backup function and real object
    _projection_original_function = m_bigworld.attr("projection");
    _projection_original_object = _projection_original_function();

    //backup initial values
    _projection_original_farPlane = pybind11::float_(_projection_original_object.attr("farPlane"));
    _projection_original_nearPlane = pybind11::float_(_projection_original_object.attr("nearPlane"));
    _projection_original_fov = pybind11::float_(_projection_original_object.attr("fov"));

    //hide projection object from others
    m_bigworld.attr("projection") = pybind11::module::import("XFW_NVIDIA").attr("ProjectionAccess");
}

void WotCamera::disableCamera()
{
    //import BigWorld module
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //restore camera
    m_bigworld.attr("camera")(_camera_original);
}

void WotCamera::disableProjection()
{
    //import BigWorld module
    pybind11::module m_bigworld = pybind11::module::import("BigWorld");

    //restore original projection values
    _projection_original_object.attr("farPlane") = _projection_original_farPlane;
    _projection_original_object.attr("nearPlane") = _projection_original_nearPlane;
    _projection_original_object.attr("fov") = _projection_original_fov;

    //restore original projection function
    m_bigworld.attr("projection") = _projection_original_function;
}

std::pair<nv::Vec3, nv::Quat> WotCamera::bigworldToAnsel(pybind11::object& cameraMatrix)
{
    //get matrix
    XMMATRIX matrix{};
    for (auto row = 0; row < _matrix_dimension; row++) {
        for (auto col = 0; col < _matrix_dimension; col++) {
            pybind11::float_ matrix_val = cameraMatrix.attr("get")(row, col);
            matrix.r[row].m128_f32[col] = static_cast<float>(matrix_val);
        }
    }

    //calculate inverse matrix
    XMMATRIX matrix_inv = XMMatrixInverse(nullptr, matrix);

    //calculate quaternion
    XMVECTOR quat = XMQuaternionRotationMatrix(matrix_inv);

    return std::pair<nv::Vec3, nv::Quat>(
        { matrix_inv.r[3].m128_f32[0], matrix_inv.r[3].m128_f32[1], matrix_inv.r[3].m128_f32[2] },
        { quat.m128_f32[0], quat.m128_f32[1], quat.m128_f32[2], quat.m128_f32[3]});
}

pybind11::object WotCamera::anselToBigworld(nv::Vec3 position, nv::Quat rotation)
{
    //create transform matrix from position vector and rotation quaternion
    const XMMATRIX matrix_inv = XMMatrixInverse(nullptr, DirectX::XMMatrixAffineTransformation(
        XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f),
        XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f),
        XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&rotation)),
        XMLoadFloat3(reinterpret_cast<const XMFLOAT3*>(&position))
    ));

    //create BigWorld matrix object
    pybind11::object o_matrix = pybind11::module::import("Math").attr("Matrix")();
    for (auto row = 0; row < _matrix_dimension; row++) {
        for (auto col = 0; col < _matrix_dimension; col++) {
            o_matrix.attr("setElement")(row, col, matrix_inv.r[row].m128_f32[col]);
        }
    }

    return o_matrix;
}
