/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

class WotReplay {

public:
    void Enable();
    void Disable();

    int GetMaxReplaySpeed();
    static void SetReplaySpeedIndex(int index);

    static bool IsReplayPlaying();

private:

    void enableCameraControl();
    void enableReplaySpeed();

    void disableCameraControl();
    void disableReplaySpeed();

    int getReplaySpeedIndex();


    bool _replay_controls_camera = false;
    int  _replay_initial_speed = 0;
    bool _is_enabled = false;
    std::vector<float> _playbackSpeeds;
};