/**
* This file is part of the XFW NVIDIA project.
* Copyright (c) 2018-2021 XVM Team.
*
* XFW NVIDIA is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* XFW NVIDIA is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "wot_avatar.h"
#include <windows.h>


pybind11::object WotAvatar::GetPlayer()
{
    auto result = pybind11::module::import("BigWorld").attr("player")();

    return result;
}

pybind11::object WotAvatar::GetPlayerAvatar()
{
    pybind11::object o_player = GetPlayer();

    if(!pybind11::hasattr(o_player, "_PlayerAvatar__isOnArena")) {
        return pybind11::none();
    }

    if (!pybind11::bool_(o_player.attr("_PlayerAvatar__isOnArena"))) {
        return pybind11::none();
    }

    return o_player;
}

pybind11::object WotAvatar::GetAvatarInputHandler()
{
    pybind11::object o_player = GetPlayerAvatar();

    if (!pybind11::hasattr(o_player, "inputHandler")) {
        return pybind11::none();
    }

    if (!pybind11::bool_(o_player.attr("inputHandler"))) {
        return pybind11::none();
    }

    auto result = o_player.attr("inputHandler");

    return result;
}

std::string WotAvatar::GetAvatarControllerName()
{
    pybind11::object o_input_handler = GetAvatarInputHandler();
    if (o_input_handler.is_none()) {
        return "";
    }

    auto result = pybind11::str(o_input_handler.attr("_AvatarInputHandler__ctrlModeName"));

    return result;
}

pybind11::object WotAvatar::GetAvatarCurrentController()
{
    pybind11::object o_input_handler = GetAvatarInputHandler();
    if (o_input_handler.is_none()) {
        return pybind11::none();
    }

    auto result = o_input_handler.attr("_AvatarInputHandler__curCtrl");

    return result;
}

bool WotAvatar::EnableAvatarCurrentController(bool enable)
{
    pybind11::object o_controller = GetAvatarCurrentController();
    if (o_controller.is_none()) {
        return false;
    }

    o_controller.attr(enable?"enable":"disable")();

    return true;
}

